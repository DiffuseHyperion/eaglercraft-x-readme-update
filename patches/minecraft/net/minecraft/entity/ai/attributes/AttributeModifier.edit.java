
# Eagler Context Redacted Diff
# Copyright (c) 2022 lax1dude. All rights reserved.

# Version: 1.0
# Author: lax1dude

> CHANGE  2 : 4  @  2 : 5

~ import net.lax1dude.eaglercraft.v1_8.EaglercraftUUID;
~ 

> INSERT  4 : 7  @  5

+ import net.lax1dude.eaglercraft.v1_8.ThreadLocalRandom;
+ import net.minecraft.util.MathHelper;
+ 

> CHANGE  7 : 8  @  4 : 5

~ 	private final EaglercraftUUID id;

> CHANGE  7 : 8  @  7 : 8

~ 	public AttributeModifier(EaglercraftUUID idIn, String nameIn, double amountIn, int operationIn) {

> CHANGE  10 : 11  @  10 : 11

~ 	public EaglercraftUUID getID() {

> EOF
