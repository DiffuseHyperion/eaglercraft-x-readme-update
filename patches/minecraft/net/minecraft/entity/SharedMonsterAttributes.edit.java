
# Eagler Context Redacted Diff
# Copyright (c) 2022 lax1dude. All rights reserved.

# Version: 1.0
# Author: lax1dude

> CHANGE  3 : 7  @  3 : 4

~ import net.lax1dude.eaglercraft.v1_8.EaglercraftUUID;
~ 
~ import net.lax1dude.eaglercraft.v1_8.log4j.LogManager;
~ import net.lax1dude.eaglercraft.v1_8.log4j.Logger;

> DELETE  11  @  8 : 10

> CHANGE  33 : 34  @  35 : 36

~ 			for (AttributeModifier attributemodifier : (Collection<AttributeModifier>) collection) {

> CHANGE  58 : 60  @  58 : 59

~ 		EaglercraftUUID uuid = new EaglercraftUUID(parNBTTagCompound.getLong("UUIDMost"),
~ 				parNBTTagCompound.getLong("UUIDLeast"));

> EOF
