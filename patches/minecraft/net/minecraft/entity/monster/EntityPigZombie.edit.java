
# Eagler Context Redacted Diff
# Copyright (c) 2022 lax1dude. All rights reserved.

# Version: 1.0
# Author: lax1dude

> CHANGE  2 : 4  @  2 : 3

~ import net.lax1dude.eaglercraft.v1_8.EaglercraftUUID;
~ 

> DELETE  3  @  2 : 3

> DELETE  3  @  4 : 6

> DELETE  2  @  4 : 5

> CHANGE  10 : 11  @  11 : 12

~ 	private static final EaglercraftUUID ATTACK_SPEED_BOOST_MODIFIER_UUID = EaglercraftUUID

> CHANGE  6 : 7  @  6 : 7

~ 	private EaglercraftUUID angerTargetUUID;

> DELETE  15  @  15 : 20

> CHANGE  64 : 65  @  69 : 70

~ 			this.angerTargetUUID = EaglercraftUUID.fromString(s);

> DELETE  83  @  83 : 106

> EOF
