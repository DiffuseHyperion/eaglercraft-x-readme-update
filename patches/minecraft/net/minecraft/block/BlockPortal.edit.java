
# Eagler Context Redacted Diff
# Copyright (c) 2022 lax1dude. All rights reserved.

# Version: 1.0
# Author: lax1dude

> CHANGE  2 : 5  @  2 : 6

~ import net.lax1dude.eaglercraft.v1_8.EaglercraftRandom;
~ import net.lax1dude.eaglercraft.v1_8.cache.EaglerLoadingCache;
~ 

> CHANGE  32 : 33  @  33 : 34

~ 	public void updateTick(World world, BlockPos blockpos, IBlockState iblockstate, EaglercraftRandom random) {

> CHANGE  120 : 121  @  120 : 121

~ 	public int quantityDropped(EaglercraftRandom var1) {

> CHANGE  15 : 16  @  15 : 16

~ 	public void randomDisplayTick(World world, BlockPos blockpos, IBlockState var3, EaglercraftRandom random) {

> CHANGE  47 : 48  @  47 : 48

~ 		EaglerLoadingCache loadingcache = BlockPattern.func_181627_a(parWorld, true);

> EOF
