
# Eagler Context Redacted Diff
# Copyright (c) 2022 lax1dude. All rights reserved.

# Version: 1.0
# Author: lax1dude

> CHANGE  2 : 4  @  2 : 5

~ import net.lax1dude.eaglercraft.v1_8.EaglercraftRandom;
~ import net.lax1dude.eaglercraft.v1_8.EaglercraftUUID;

> CHANGE  101 : 102  @  102 : 103

~ 	public static int getRandomIntegerInRange(EaglercraftRandom parRandom, int parInt1, int parInt2) {

> CHANGE  4 : 5  @  4 : 5

~ 	public static float randomFloatClamp(EaglercraftRandom parRandom, float parFloat1, float parFloat2) {

> CHANGE  4 : 5  @  4 : 5

~ 	public static double getRandomDoubleInRange(EaglercraftRandom parRandom, double parDouble1, double parDouble2) {

> CHANGE  148 : 149  @  148 : 149

~ 	public static EaglercraftUUID getRandomUuid(EaglercraftRandom rand) {

> CHANGE  3 : 4  @  3 : 4

~ 		return new EaglercraftUUID(i, j);

> EOF
