
# Eagler Context Redacted Diff
# Copyright (c) 2022 lax1dude. All rights reserved.

# Version: 1.0
# Author: lax1dude

> CHANGE  2 : 3  @  2 : 21

~ import net.lax1dude.eaglercraft.v1_8.internal.PlatformApplication;

> DELETE  3  @  21 : 25

> CHANGE  1 : 3  @  5 : 7

~ 	public static IChatComponent saveScreenshot() {
~ 		return new ChatComponentText("Saved Screenshot As: " + PlatformApplication.saveScreenshot());

> DELETE  3  @  3 : 79

> EOF
