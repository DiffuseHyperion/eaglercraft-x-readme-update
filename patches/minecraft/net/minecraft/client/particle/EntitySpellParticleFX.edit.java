
# Eagler Context Redacted Diff
# Copyright (c) 2022 lax1dude. All rights reserved.

# Version: 1.0
# Author: lax1dude

> CHANGE  2 : 4  @  2 : 6

~ import net.lax1dude.eaglercraft.v1_8.EaglercraftRandom;
~ import net.lax1dude.eaglercraft.v1_8.opengl.WorldRenderer;

> CHANGE  7 : 8  @  9 : 10

~ 	private static final EaglercraftRandom RANDOM = new EaglercraftRandom();

> EOF
