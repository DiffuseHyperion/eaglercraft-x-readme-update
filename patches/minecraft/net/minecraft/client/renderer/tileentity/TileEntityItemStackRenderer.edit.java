
# Eagler Context Redacted Diff
# Copyright (c) 2022 lax1dude. All rights reserved.

# Version: 1.0
# Author: lax1dude

> CHANGE  2 : 6  @  2 : 4

~ import net.lax1dude.eaglercraft.v1_8.EaglercraftUUID;
~ 
~ import net.lax1dude.eaglercraft.v1_8.mojang.authlib.GameProfile;
~ import net.lax1dude.eaglercraft.v1_8.opengl.GlStateManager;

> DELETE  5  @  3 : 6

> CHANGE  31 : 32  @  34 : 35

~ 					gameprofile = new GameProfile((EaglercraftUUID) null, nbttagcompound.getString("SkullOwner"));

> EOF
