
# Eagler Context Redacted Diff
# Copyright (c) 2022 lax1dude. All rights reserved.

# Version: 1.0
# Author: lax1dude

> CHANGE  2 : 7  @  2 : 4

~ import net.lax1dude.eaglercraft.v1_8.internal.buffer.FloatBuffer;
~ import net.lax1dude.eaglercraft.v1_8.EaglercraftRandom;
~ 
~ import net.lax1dude.eaglercraft.v1_8.opengl.GlStateManager;
~ import net.lax1dude.eaglercraft.v1_8.opengl.WorldRenderer;

> DELETE  8  @  5 : 6

> DELETE  1  @  2 : 4

> CHANGE  7 : 8  @  9 : 10

~ 	private static final EaglercraftRandom field_147527_e = new EaglercraftRandom(31100L);

> CHANGE  35 : 36  @  35 : 36

~ 			float f7 = (float) (-(d1 + (double) f3 - 1.25));

> CHANGE  14 : 15  @  14 : 18

~ 			GlStateManager.enableTexGen();

> CHANGE  35 : 36  @  38 : 42

~ 		GlStateManager.disableTexGen();

> EOF
