
# Eagler Context Redacted Diff
# Copyright (c) 2022 lax1dude. All rights reserved.

# Version: 1.0
# Author: lax1dude

> CHANGE  2 : 5  @  2 : 3

~ import net.lax1dude.eaglercraft.v1_8.opengl.EaglercraftGPU;
~ import net.lax1dude.eaglercraft.v1_8.opengl.GlStateManager;
~ import net.lax1dude.eaglercraft.v1_8.opengl.WorldRenderer;

> DELETE  4  @  2 : 5

> DELETE  4  @  7 : 8

> CHANGE  41 : 42  @  42 : 43

~ 		EaglercraftGPU.glNormal3f(f10, 0.0F, 0.0F);

> CHANGE  7 : 8  @  7 : 8

~ 		EaglercraftGPU.glNormal3f(-f10, 0.0F, 0.0F);

> CHANGE  10 : 11  @  10 : 11

~ 			EaglercraftGPU.glNormal3f(0.0F, 0.0F, f10);

> EOF
