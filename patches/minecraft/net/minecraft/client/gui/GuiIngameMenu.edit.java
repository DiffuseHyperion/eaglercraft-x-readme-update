
# Eagler Context Redacted Diff
# Copyright (c) 2022 lax1dude. All rights reserved.

# Version: 1.0
# Author: lax1dude

> CHANGE  2 : 3  @  2 : 9

~ import net.lax1dude.eaglercraft.v1_8.Mouse;

> DELETE  5  @  11 : 12

> DELETE  2  @  3 : 5

> DELETE  2  @  4 : 5

> CHANGE  20 : 21  @  21 : 22

~ 		guibutton.enabled = false;

> CHANGE  3 : 4  @  3 : 4

~ 	protected void actionPerformed(GuiButton parGuiButton) {

> DELETE  7  @  7 : 8

> DELETE  5  @  6 : 9

> CHANGE  18 : 19  @  21 : 22

~ 			break;

> CHANGE  7 : 10  @  7 : 8

~ 		if (Mouse.isActuallyGrabbed()) {
~ 			Mouse.setGrabbed(false);
~ 		}

> EOF
