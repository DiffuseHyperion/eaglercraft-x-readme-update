
# Eagler Context Redacted Diff
# Copyright (c) 2022 lax1dude. All rights reserved.

# Version: 1.0
# Author: lax1dude

> CHANGE  2 : 4  @  2 : 3

~ import net.lax1dude.eaglercraft.v1_8.opengl.GlStateManager;
~ import net.lax1dude.eaglercraft.v1_8.opengl.OpenGlHelper;

> DELETE  6  @  5 : 7

> DELETE  1  @  3 : 4

> INSERT  49 : 50  @  50

+ 		GlStateManager.enableDepth();

> INSERT  3 : 4  @  2

+ 		GlStateManager.disableDepth();

> CHANGE  43 : 44  @  42 : 43

~ 	protected void actionPerformed(GuiButton parGuiButton) {

> EOF
