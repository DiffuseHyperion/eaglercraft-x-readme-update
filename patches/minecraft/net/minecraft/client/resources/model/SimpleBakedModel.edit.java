
# Eagler Context Redacted Diff
# Copyright (c) 2022 lax1dude. All rights reserved.

# Version: 1.0
# Author: lax1dude

> DELETE  2  @  2 : 3

> INSERT  1 : 5  @  2

+ 
+ import com.google.common.collect.Lists;
+ 
+ import net.lax1dude.eaglercraft.v1_8.minecraft.EaglerTextureAtlasSprite;

> DELETE  8  @  4 : 6

> CHANGE  7 : 8  @  9 : 10

~ 	protected final EaglerTextureAtlasSprite texture;

> CHANGE  4 : 5  @  4 : 5

~ 			EaglerTextureAtlasSprite parTextureAtlasSprite, ItemCameraTransforms parItemCameraTransforms) {

> CHANGE  29 : 30  @  29 : 30

~ 	public EaglerTextureAtlasSprite getParticleTexture() {

> CHANGE  12 : 13  @  12 : 13

~ 		private EaglerTextureAtlasSprite builderTexture;

> CHANGE  8 : 9  @  8 : 9

~ 		public Builder(IBakedModel parIBakedModel, EaglerTextureAtlasSprite parTextureAtlasSprite) {

> CHANGE  12 : 13  @  12 : 13

~ 		private void addFaceBreakingFours(IBakedModel parIBakedModel, EaglerTextureAtlasSprite parTextureAtlasSprite,

> CHANGE  8 : 10  @  8 : 9

~ 		private void addGeneralBreakingFours(IBakedModel parIBakedModel,
~ 				EaglerTextureAtlasSprite parTextureAtlasSprite) {

> CHANGE  31 : 32  @  30 : 31

~ 		public SimpleBakedModel.Builder setTexture(EaglerTextureAtlasSprite parTextureAtlasSprite) {

> EOF
