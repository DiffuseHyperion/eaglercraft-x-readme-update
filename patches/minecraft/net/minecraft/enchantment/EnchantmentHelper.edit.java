
# Eagler Context Redacted Diff
# Copyright (c) 2022 lax1dude. All rights reserved.

# Version: 1.0
# Author: lax1dude

> DELETE  2  @  2 : 4

> CHANGE  6 : 11  @  8 : 11

~ import net.lax1dude.eaglercraft.v1_8.EaglercraftRandom;
~ 
~ import com.google.common.collect.Lists;
~ import com.google.common.collect.Maps;
~ 

> CHANGE  18 : 19  @  16 : 17

~ 	private static final EaglercraftRandom enchantmentRand = new EaglercraftRandom();

> CHANGE  212 : 214  @  212 : 213

~ 	public static int calcItemStackEnchantability(EaglercraftRandom parRandom, int parInt1, int parInt2,
~ 			ItemStack parItemStack) {

> CHANGE  16 : 17  @  15 : 16

~ 	public static ItemStack addRandomEnchantment(EaglercraftRandom parRandom, ItemStack parItemStack, int parInt1) {

> CHANGE  8 : 9  @  8 : 9

~ 			for (EnchantmentData enchantmentdata : (List<EnchantmentData>) list) {

> CHANGE  12 : 14  @  12 : 13

~ 	public static List<EnchantmentData> buildEnchantmentList(EaglercraftRandom randomIn, ItemStack itemStackIn,
~ 			int parInt1) {

> CHANGE  32 : 33  @  31 : 32

~ 							for (EnchantmentData enchantmentdata1 : (List<EnchantmentData>) arraylist) {

> EOF
