
# Eagler Context Redacted Diff
# Copyright (c) 2022 lax1dude. All rights reserved.

# Version: 1.0
# Author: lax1dude

> CHANGE  3 : 5  @  3 : 4

~ import net.lax1dude.eaglercraft.v1_8.EaglercraftRandom;
~ 

> DELETE  7  @  6 : 9

> CHANGE  82 : 83  @  85 : 86

~ 	public WeightedRandomChestContent getRandom(EaglercraftRandom rand) {

> CHANGE  4 : 5  @  4 : 5

~ 	public WeightedRandomChestContent getRandom(EaglercraftRandom rand, int minChance, int maxChance, int weight) {

> EOF
